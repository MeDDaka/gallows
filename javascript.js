let words = [
  "Капуста",
  "Жизнь",
  "Деревня",
  "Магазин",
  "Супермаркет",
  "Мясо",
  "Глагол",
  "Череп",
  "Туалет",
  "Чай",
  "Тыква",
  "Лошадь",
  "Повозка",
];

let word = words[Math.floor(Math.random() * words.length)].toLowerCase(); // choose random word
let arrWord = word.split("");
let emptyWord = [];
let gameState = true;
let amountOfTry = 6;
let canvas = document.getElementById('game');
let ctx    =   canvas.getContext('2d');

ctx.lineWidth = 3;
ctx.strokeRect(100,0,50,50);

ctx.beginPath();
// neck
ctx.moveTo(125,50);
ctx.lineTo(125,150);
//neck

//hands
//  left hands
ctx.moveTo(125,95);
ctx.lineTo(70,45);
//rigth hands
ctx.moveTo(125,95);
ctx.lineTo(175,45);
//hands

//legs
//rigth leg
ctx.moveTo(125,150);
ctx.lineTo(175,220);
//left leg
ctx.moveTo(125,150);
ctx.lineTo(70,220);
//legs

ctx.stroke();

let game = {

  amountOfTry : 6,
  
  
  

  draw() {

    if (this.amountOfTry == 5){

      ctx.lineWidth = 3;
      ctx.strokeRect(150,40,50,50);
    
    } else if (this.amountOfTry == 4){
    
      ctx.beginPath();
      
      ctx.stroke();

    } else if (this.amountOfTry == 3){
      //Рисуем тело
    }else if (this.amountOfTry == 2){
      //Рисуем руки
    }else if (this.amountOfTry == 1){
      //Рисуем ноги
    }

  }

}

game.amountOfTry = 4;
game.draw();


for (i = 0; i < word.length; i++) {
  emptyWord[i] = "-";
}

const checkLetter = (letter) => {
  let trueLetter = false;

  for (let j = 0; j < arrWord.length; j++) {
    if (arrWord[j] == letter) {
      // check letter from arrWord[j]
      if (emptyWord[j] == '-'){
         emptyWord[j] = letter;
         trueLetter = true;
      }else {
        alert('Вы уже угадали эту букву! минус бал!');
        trueLetter = false;
      }
      
      
    }
  }

  if (trueLetter == false) {
    // if the letter didn't find: amount of try - 1
    amountOfTry--;
  } else if (amountOfTry < 6) {
    // else if the letter find and amount of try not a full: amount of try + 1;
    amountOfTry++;
  }

  if (word == emptyWord.join("").toLowerCase()) {
    alert(`Вы победили ваше слово - ${emptyWord.join("")}`);
    return false;
  } else return true;
};

const gamePlay = function () {
  while (gameState) {

    if (amountOfTry <= 0) {
      //if try are over then game off
      gameState = false;
      alert(`Вы проиграли, слово: ${word.toUpperCase()}`);
      break;
    }

    let userLetter = prompt("Введите одну букву");

    if (userLetter == null) {
      alert("Вы вышли из игры");
      gameState = false;
      break;
    } else if (userLetter.length > 1) {
      alert("Введите только одну букву");
    } else {
      gameState = checkLetter(userLetter);
      if (gameState == false) {
        break;
      }
      alert(
        emptyWord.join("").toUpperCase() + ` Осталось попыток: ${amountOfTry}`
      );
    }
  }
};

// gamePlay();


